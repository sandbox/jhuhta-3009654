# Paragraphs role access

A simple module that provides the ability to limit access to a paragraph type by
user roles.

## Introduction

Paragraphs role access module adds form elements to paragraph type form, and
makes it possible to configure limitations to access (create, update, view,
delete) the paragraph type.

For example, if creating is forbidden without a certain role, the users having
that role don't see that paragraph type in the widget that lists the paragraph
types available. Also, if editing is not allowed for a user, they might still
see the paragraph's presence on the form (and perhaps add another ones), but not
edit it.

The module also adds a permission called 'bypass paragraphs role access' to
bypass all checks made by this module.

## Requirements

As this module enhances [Paragraphs][1] module for [Drupal 8][2], those are the
obvious requirements. There's no other requirements.

## Installing

Install as you would [normally install][3] a contributed Drupal module.

## Configuration

Enable the module and create a new paragraph type or edit an old one. Add those
access limitations you need. 

The module configuration is stored as part of the paragraph types' configuration. 
It uses Drupal's third party setting mechanism to store the information, and other 
parts of Drupal or Paragraphs configuration are not messed with. Thus the
uninstallation will be no problem either.


## Credits

Original module was developed by [Jukka Huhta][4] of [Siili Solutions][5] in an
[Aalto University][6] project.

Some ideas are loaned from paragraphs_type_permissions submodule, but the
approach is a bit different.


[1]: https://www.drupal.org/project/paragraphs
[2]: https://www.drupal.org/docs/8
[3]: https://www.drupal.org/documentation/install/modules-themes/modules-8
[4]: https://www.drupal.org/u/jhuhta
[5]: https://www.siili.com/
[6]: https://www.aalto.fi/
